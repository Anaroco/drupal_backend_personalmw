<?php

namespace Drupal\rocks_backend\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Rocks backend routes.
 */
class BartController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['texto_plano'] = [
       '#plain_text' => 'Multiplicate por cero!',
   ];
    $build['enlace'] = [
       '#type' => 'link',
       '#url' => \Drupal\Core\Url::fromRoute('<front>'),
       '#title' => t('Click'),
    ];

    $build['plantilla'] = [
       '#theme' => 'time',
       '#timestamp' => time(),
    ];

    $build['templeta'] = [
       '#type' => 'inline_template',
       '#template' => "<p>{{ 'now'|date('d-m-y') }}</p>",
       '#title' => t('Click'),
     ];

   return $build;
  }

}
