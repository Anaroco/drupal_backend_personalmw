<?php

namespace Drupal\rocks_backend\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;


/**
 * Configure Backend rocks settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backend_rocks_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['backend_rocks.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('backend_rocks.settings');
    $options = [
      'Hombre' => t('H'),
      'Mujer' => t('M'),
    ];
    $form['sexo'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sexo'),
      '#options' => $options,
      '#default_value' => $this->config('backend_rocks.settings')->get('sexo'),
    ];
    $form['telefono'] = [
      '#type' => 'tel',
      '#title' => $this->t('Telefono'),
      '#default_value' => $this->config('backend_rocks.settings')->get('telefono'),
    ];
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activado'),
      '#default_value' => $this->config('backend_rocks.settings')->get('enabled'),
      /*'#ajax' => [
        'callback' => '::alertCallback',
        'wrapper' => 'ajax-forms-example',
      ]*/
    ];
    $form['ajax_wrapper'] = [
      '#markup' => '<div id="ajax-forms-example">AQUI VA MI RESPUESTA AJAX</div>'
    ];
    $form['color_preview'] = [
      '#type' => 'inline_template',
      '#template' => sprintf('<div id="color-preview" style="padding: 1rem; display: inline-block; width: 64px; height: 64px; background-color: {{color}}">&nbsp;</div>'),
      '#context' => [
        'color' => $form_state->getValue('color', $config->get('color')),
      ]
    ];
    $form['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color'),
      '#default_value' => $this->config('backend_rocks.settings')->get('color'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form = parent::buildForm($form, $form_state);

    $form['actions']['submit']['#states'] = [
      'visible' => [
        ':input[name="color"]' => ['filled' => TRUE],
      ],
    ];
    $form['actions'] += ['#weight' => 50];
    $form['actions']['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('Preview'),
      '#button_type' => 'secondary',
      '#ajax' => [
        'callback' => '::alertCallback',
        'wrapper' => 'color-preview',
      ],
    ];

    $form['actions']['preview_cmd'] = [
      '#type' => 'button',
      '#value' => $this->t('Preview via Command'),
      '#button_type' => 'secondary',
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'color-preview',
      ],
    ];

    //dpm($form);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $color = $form_state->getValue('color');
    if (empty($color)) {
      $form_state->setErrorByName('color', $this->t('The field is required.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('backend_rocks.settings')
      ->set('sexo', $form_state->getValue('sexo'))
      ->set('telefono', $form_state->getValue('telefono'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('color', $form_state->getValue('color'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  public function alertCallback(array &$form, FormStateInterface $formState): array{
    return $form['color_preview'];
  }

  public function ajaxCallback(array &$form, FormStateInterface $formState): AjaxResponse{
    $form['actions']['submit']['#value'] = 'Que pasa colega';
    $response = new AjaxResponse();
    $response
      ->addCommand(new ReplaceCommand(NULL, $form['color_preview']))
      ->addCommand(new ReplaceCommand('#edit-submit', $form['actions']['submit']))
      ->addCommand(new AlertCommand('Hola'));
    return $response;
  }

}


