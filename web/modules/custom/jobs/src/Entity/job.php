<?php
namespace Drupal\jobs\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\jobs\JobInterface;

/**
 * Defines the job entity class.
 *
 * @ContentEntityType (
 *    id = "job",
 *    label = @Translation("Empleo"),
 *    handlers = {
 *      "views_data" = "Drupal\views\EntityViewsData",
 *    },
 *    base_table = "job",
 *    entity_keys = {
 *        "id" = "id",
 *        "label" = "label",
 *        "uuid" = "uuid",
 *   },
 * )
 */

class Job extends ContentEntityBase implements JobInterface {

  /**
   *  {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled');

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time the job was created'));

      return $fields;

  }

}

