<?php

namespace Drupal\jobs;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a job entity type.
 */
interface JobInterface extends ContentEntityInterface {

}
